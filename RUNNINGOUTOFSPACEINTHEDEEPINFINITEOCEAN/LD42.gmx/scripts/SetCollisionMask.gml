///SetCollisionMask(Sprite, Width, Height, Distance);

//getting the mask size:
Width=argument1;
Height=argument2;
distance=argument3;

//copy the sprite to the variable:
sprite=sprite_duplicate(argument0);

//change the object sprite:
self.sprite_index=sprite;
self.image_xscale=(Width+distance*2)/self.sprite_width;
self.image_yscale=(Height+distance*2)/self.sprite_height;
//change the collision rectangle mask:
sprite_set_offset(self.sprite_index,
    sprite_get_xoffset(self.sprite_index)+distance/self.image_xscale,
    sprite_get_yoffset(self.sprite_index)+distance/self.image_yscale);
