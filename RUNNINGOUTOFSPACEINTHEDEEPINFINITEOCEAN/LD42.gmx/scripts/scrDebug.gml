var kRestart, kExit, kPrev, kNext;
kKill = keyboard_check_pressed(ord('K'));
kRestart = keyboard_check_pressed(ord('R'));
kPhysicDebug = keyboard_check(ord('P'));

kExit    = keyboard_check_pressed(vk_escape);
kPrev    = keyboard_check_pressed(vk_subtract);
kNext    = keyboard_check_pressed(vk_add);
if (kRestart)
    room_restart();
if (kExit)
    game_end();    
// Iterate through rooms backward
if (kPrev) {
    if (room == room_first)
        room_goto(room_last);
    else
        room_goto_previous();
}
// Iterate through rooms forwards
if (kNext) {
    if (room == room_last)
        room_goto(room_first);
    else
        room_goto_next();
}
if (kKill) {
if instance_exists(oPlayer)
with oPlayer instance_destroy()
}
///todo

if keyboard_check_pressed(vk_f5) instance_create(0,0,oScreenBack)
if keyboard_check_pressed(vk_f1) instance_create(mouse_x,mouse_y,oFishAI)
if keyboard_check_pressed(vk_f2) instance_create(mouse_x,mouse_y,oTrashBag)
if keyboard_check_pressed(vk_f3) instance_create(mouse_x,mouse_y,oFood)
if keyboard_check_pressed(vk_f4) instance_create(mouse_x,mouse_y,oGoupFish)
///dash !
if keyboard_check_pressed(ord('E')) {
oPlayer.dashBar=1000
oPlayer.kDash=true
}
