fix = physics_fixture_create();
if argument0="circle_shape"
physics_fixture_set_circle_shape(fix, sprite_width*abs(image_xscale)/2);//idkWARNING: Too many manifolds in collision
if argument0="box_shape"
physics_fixture_set_box_shape(fix, sprite_width*abs(image_xscale)/2,sprite_height*abs(image_xscale)/2);
if argument0="box_shape_sensor"{
physics_fixture_set_circle_shape(fix, sprite_width*abs(image_xscale)/2);//idkWARNING: Too many manifolds in collision
physics_fixture_set_sensor(fix, 1); 
}
physics_fixture_set_density(fix, argument1);
physics_fixture_set_restitution(fix, argument2);
physics_fixture_set_linear_damping(fix, argument3);
physics_fixture_set_angular_damping(fix, argument4);
physics_fixture_set_friction(fix, argument5);
inst = id

my_fix = physics_fixture_bind(fix, inst);

physics_fixture_delete(fix);



return fix ;
