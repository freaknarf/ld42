//Below is particle code:

Sblood = part_system_create();
particle1 = part_type_create();
part_type_shape(particle1,pt_shape_pixel);
part_type_size(particle1,1,1,0.10,0.10);
part_type_scale(particle1,0.10,0.10);
part_type_color1(particle1,128);
part_type_speed(particle1,0.50,1,0,0);
part_type_direction(particle1,0,359,0,0);
part_type_gravity(particle1,0.10,270);
part_type_orientation(particle1,0,0,0,1,0);
part_type_blend(particle1,0);
part_type_life(particle1,30,60);

emitter1 = part_emitter_create(Sblood);
part_emitter_region(Sname,emitter1,x,x,y,y,0,0);
part_emitter_stream(Sname,emitter1,particle1,30);
