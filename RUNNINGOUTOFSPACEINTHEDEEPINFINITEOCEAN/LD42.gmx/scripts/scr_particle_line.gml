Slines = part_system_create();
particle1 = part_type_create();
part_type_shape(particle1,pt_shape_line);
part_type_size(particle1,.1,.2,0,0);
part_type_scale(particle1,1,0.10);
part_type_color1(particle1,16777215);
part_type_speed(particle1,0,0,0,0);
part_type_direction(particle1,0,0,0,0);
part_type_gravity(particle1,0,270);
part_type_orientation(particle1,image_angle ,image_angle,0,1,0);
part_type_blend(particle1,0);
part_type_life(particle1,3,8);
emitter1 = part_emitter_create(Slines);

part_emitter_region(Slines,emitter1,x,x,y-sprite_height/2,y+sprite_height/2,ps_shape_line,1);
part_emitter_burst(Slines,emitter1,particle1,argument0);


