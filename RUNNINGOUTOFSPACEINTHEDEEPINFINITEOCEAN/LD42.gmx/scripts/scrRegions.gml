
with  (oAI)
if isOutsideBigView(){
   instance_deactivate_object(id)
   phy_active=0
   }
   else
   phy_active=1
 /* 
with  (oWall)
if isOutsideBigViewExt(64,64)
{
   instance_deactivate_object(id)
   phy_active=0
   }
   else
   phy_active=1
*/
with  (oParGroup)
if isOutsideBigView()
   instance_deactivate_object(id)

   
with  (oTrashBag)
if isOutsideBigView(){
   instance_deactivate_object(id)
   phy_active=0
   }
   else
   phy_active=1
   
with  (oSpawnTrash)
if isOutsideBigView()
   instance_deactivate_object(id)


with  (oProps)
if isOutsideView()
instance_deactivate_object(id)    

//activates 8 screens around (think like activating outside of the center screen of the hash: # ... get it ?) 

if global.Config = 0
instance_activate_region(view_xview[0]-view_wview[0],view_yview[0]-view_hview[0],3*view_wview[0],3*view_hview[0],1)
else //activates 8  screens around (think like activating outside of the center screen of the hash: # ... get it ?) 
instance_activate_region(view_xview[0]-view_wview[0]/2,view_yview[0]-view_hview[0]/2,2*view_wview[0],2*view_hview[0],1)

