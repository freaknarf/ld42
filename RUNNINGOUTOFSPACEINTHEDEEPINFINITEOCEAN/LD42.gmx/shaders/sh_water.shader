//
// Simple passthrough vertex shader
//
attribute vec3 in_Position;
//
attribute vec4 in_Colour;
attribute vec2 in_TextureCoord;

varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
 vec4 object_space_pos = vec4( in_Position.x,in_Position.y,in_Position.z,1.0);
 gl_Position=gm_Matrices[MATRIX_WORLD_VIEW_PROJECTION] * object_space_pos;
 v_vColour = in_Colour;
 v_vTexcoord = in_TextureCoord;
}
//######################_==_YOYO_SHADER_MARKER_==_######################@~//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform float Time;
uniform vec2 Texel;
const float Xspeed=0.001;
const float Xfreq=1.0;
const float Xsize=10.0;
const float Yfreq=1.0;
const float Ysize=30.0;
void main()
{
    float Xwave=sin(Time*Xspeed+v_vTexcoord.y*Xfreq)*(Xsize*Texel.x) * v_vTexcoord.y;
    float Ywave=sin(Time*Xspeed+v_vTexcoord.y*Yfreq)*(Xsize*Texel.y) * v_vTexcoord.y;
    gl_FragColor=v_vColour * texture2D(gm_BaseTexture,v_vTexcoord+vec2(Xwave,Ywave));
}
